#!/bin/bash
# this script should be run before a node-specific script.
# it installs and configures everything needed to set up a kubernetes node.

# exit script if any command fails
set -e
USAGE="Usage: $0 [options]\n
\t-s\t\tPermanently disable swap if enabled\n
\t-h\t\tUsage information\n
\t-i <interface>\tthe interface to use for network configuration"
PERM_DISABLE_SWAP="false"
IFACE="$(route | awk '/^default/{print $NF}')"

#get options
while getopts ":shi:" opt; do
  case ${opt} in
    s )
      PERM_DISABLE_SWAP="true"
      ;;
    h )
    echo -e $USAGE
    exit 0
      ;;
    i )
      IFACE=$OPTARG
      ;;
    \? )
      echo "$0: Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "$0: Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done

# since we need to do some things as root, this script will exit if it is not executed as root.
if [ "$(id -u)" != "0" ]; then
  echo "$0: please run this script as root: sudo $0 $@" 1>&2
  exit 1
fi

# swap needs to be disabled for the kubelets to work correctly,
# so we check if there are swap files, and disable swap if needed.
if [ $(cat /proc/swaps | wc -l) -gt 1 ]; then
  if [ "$PERM_DISABLE_SWAP" == "true" ]; then
    sed -e '/swap/ s/^#*/#/' -i /etc/fstab
    echo "$0: swap disabled permanently, uncomment the swap line in /etc/fstab to re-enable."
  else
  echo "$0: kubelet won't work properly when swap is enabled, and swap will need to be disabled to continue."
  echo "$0: type 'temp' to disable swap temporarily, 'perm' to disable it permanently, or 'abort' to abort."
    while read line; do
      if [ "$line" == "temp" ]; then
        swapoff -a
        echo "$0: swap disabled temporarily, you will need to run 'swapoff -a' after every reboot!"
        break
      elif [ "$line" == "perm" ]; then
        swapoff -a
        sed -e '/swap/ s/^#*/#/' -i /etc/fstab
        echo "$0: swap disabled permanently, uncomment the swap line in /etc/fstab to re-enable."
        break
      elif [ "$line" == "abort" ]; then
        echo "$0: aborting!"
        exit 0
      else
        echo "invalid input. expected one of 'temp', 'perm', or 'abort'." 1>&2
      fi
    done
  fi
fi
#get the ip for this node for network configuration
NODE_IP="$(ip -br a show "$IFACE" | grep -E -o "([0-9]{1,3}\.){3}[0-9]{1,3}")"
if [ -z "$NODE_IP" ]; then
	echo "$0: could not retrieve IP address for interface $IFACE" 1>&2
	exit 1
fi

# update hosts file
sed -e "s/^.*${HOSTNAME}.*/${NODE_IP} ${HOSTNAME} ${HOSTNAME}.local/" -i /etc/hosts

# remove ubuntu-bionic entry
sed -e '/^.*ubuntu-bionic.*/d' -i /etc/hosts

#check correct iptables configuration for bridge traffic, and configure if needed
if [ -z "$(lsmod | grep br_netfilter)" ]; then
  echo "----- configuring iptables for bridge traffic -----"
  cat <<EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
  sysctl --system
fi

# add official sources for kubernetes and docker
echo "----- install required packages -----"
apt-get update
apt-get install -y apt-transport-https curl ca-certificates software-properties-common gnupg2
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
cat <<EOF | tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
# install all required packages
apt-get update
apt-get install -y containerd.io=1.2.13-2 \
docker-ce=5:19.03.11~3-0~ubuntu-$(lsb_release -cs) \
docker-ce-cli=5:19.03.11~3-0~ubuntu-$(lsb_release -cs) \
kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
# set up the docker daemon
echo "----- creating docker daemon -----"
if [ ! -f /etc/docker/daemon.json ]; then
  cat <<EOF | tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
fi

if [ ! -d /etc/systemd/system/docker.service.d ]; then
mkdir -p /etc/systemd/system/docker.service.d
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl enable docker
fi

#check if everything went ok
if [ "$?" != "0" ]; then
  echo "$0: prerequisite setup failed." 1>&2
  exit 1
else
  echo "$0: prerequisite setup completed!"
fi
