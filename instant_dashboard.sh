#!/bin/bash
kubectl apply -f nodeport-kubernetes-dashboard.yaml
echo -n "waiting for dashboard to be ready. This can take a while.."
READY=0
while [ $READY -lt 1 ]; do
  sleep 1
  echo -n '.'
  READY=$(kubectl get deployment -n kubernetes-dashboard | grep kubernetes-dashboard | awk '{print $2}' | cut -d '/' -f1)
done
echo
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}') | grep 'token:'
NODE_IP="$(kubectl get nodes -o wide | grep $(kubectl get pods -n kubernetes-dashboard -o wide | grep kubernetes-dashboard | awk '{print $7}') | awk '{print $6}')"
SERVICE_PORT="$(kubectl get svc -n kubernetes-dashboard -o wide | grep kubernetes-dashboard | awk -F '[:/]' '{print $2}')"
echo "you can use the token above to log in on the dashboard on this address:"
echo "https://$NODE_IP:$SERVICE_PORT"
