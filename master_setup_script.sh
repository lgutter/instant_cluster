#!/bin/bash
# this is a setup script to create a kubernetes master node using kubeadm.
# it assumes all the required packages are installed and configured.
# this can be done with pre_setup_script.sh.
# it also leaves some artifacts when running from vagrant. (with -v option)

# exit script if any command fails
set -e

USAGE="Usage:	$0 [options]\n
\t-i <interface>\tThe network interface to expose the API server on\n
\t-p <CIDR>\tspecify a specific pod network CIDR\n
\t-v\t\tscript was started from vagrant, so copy artifacts to /vagrant/\n
\t-h\t\tUsage information"
API_IFACE="$(route | awk '/^default/{print $NF}')"
POD_CIDR="10.244.0.0/16"
LOG_DIR="/tmp"
# parse options
while getopts ":i:p:hv" opt; do
  case ${opt} in
    i )
      API_IFACE=$OPTARG
      ;;
    p )
      POD_CIDR=$OPTARG
      ;;
    v )
      LOG_DIR="/vagrant"
      ;;
    h )
	  echo -e $USAGE
	  exit 0
      ;;
    \? )
      echo "$0: Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "$0: Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done

# since we need to do some things as root, this script will exit if it is not executed as root.
if [ "$(id -u)" != "0" ]; then
	echo "$0: please run this script as root: sudo $0 $@" 1>&2
	exit 1
fi
#get the ip for this node where we will expose the API Server
API_IP="$(ip -br a show "$API_IFACE" | grep -E -o "([0-9]{1,3}\.){3}[0-9]{1,3}")"
if [ -z "$API_IP" ]; then
	echo "$0: could not retrieve IP address for interface $API_IFACE" 1>&2
	exit 1
fi

# start setup of the master node with kubeadm
echo "running kubeadm init with Pod CIDR $POD_CIDR and API IP Adress $API_IP..." | tee "$LOG_DIR/kubeadm_init.log"
kubeadm init --pod-network-cidr="$POD_CIDR" --apiserver-advertise-address="$API_IP" | tee "$LOG_DIR/kubeadm_init.log"
export KUBECONFIG=/etc/kubernetes/admin.conf
#install a CNI plugin
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
if [ "$?" != "0" ]; then
	echo "$0: installation of master node failed!" 1>&2
	exit 1
fi
KUBEADM_JOIN="$(cat -s "$LOG_DIR/kubeadm_init.log" | grep -A1 "kubeadm join" | tr -d '\' | tr -d '\n')"
echo -e "#!/bin/bash\n$KUBEADM_JOIN" > "$LOG_DIR/kubeadm_join.sh"
chmod +x "$LOG_DIR/kubeadm_join.sh"
echo "---------- Installation succesful! ----------"
cp /etc/kubernetes/admin.conf "$LOG_DIR/admin.conf"
echo "$0: copied admin.conf to $LOG_DIR."
echo "$0: You can now set up your worker nodes using the following command:"
echo "$KUBEADM_JOIN"
