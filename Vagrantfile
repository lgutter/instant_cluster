# -*- mode: ruby -*-
# vi:set ft=ruby sw=2 ts=2 sts=2:

# Vagrant file that can set up a local kubernetes cluster with a master node
# and multiple worker nodes using set up scripts.

# Define the number of worker nodes.
NUM_WORKER_NODE = 1

IP_NW = "192.168.56."
MASTER_IP = 2
NODE_IP_START = MASTER_IP

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version.
# Please don't change it unless you know what you're doing.
Vagrant.configure("2") do |config|
  # The vagrant box (base image) we are using for the nodes
  config.vm.box = "ubuntu/bionic64"

  # disable automatic updating of VMs since this could break the nodes
  config.vm.box_check_update = false

  # Provision Master Node
  config.vm.define "kubemaster" do |node|
    node.vm.provider "virtualbox" do |vb|
        vb.name = "kubemaster"
        vb.memory = 2048
        vb.cpus = 2
    end
    node.vm.hostname = "kubemaster"
    node.vm.network :private_network, ip: IP_NW + "#{MASTER_IP}"
    node.vm.provision "pre-setup", type: "shell",
    path: "pre_setup_script.sh", privileged: true, args: ["-s", "-i", "enp0s8"]
    node.vm.provision "master-setup", type: "shell",
    path: "master_setup_script.sh", privileged: true, args: ["-v", "-i", "enp0s8"]
  end


  # Provision Worker Nodes
  (1..NUM_WORKER_NODE).each do |i|
    config.vm.define "kubenode#{i}" do |node|
      node.vm.provider "virtualbox" do |vb|
        vb.name = "kubenode#{i}"
        vb.memory = 2048
        vb.cpus = 2
      end
    node.vm.hostname = "kubenode#{i}"
    node.vm.network :private_network, ip: IP_NW + "#{NODE_IP_START + i}"
    node.vm.provision "pre-setup", type: "shell",
    path: "pre_setup_script.sh", privileged: true, args: ["-s", "-i", "enp0s8"]
    node.vm.provision "worker-setup", type: "shell",
    path: "kubeadm_join.sh", privileged: true
    end
  end
end
